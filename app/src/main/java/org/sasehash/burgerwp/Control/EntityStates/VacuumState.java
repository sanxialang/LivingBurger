/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.EntityStates;

import org.sasehash.burgerwp.Control.Actions.CollisionAction;
import org.sasehash.burgerwp.Control.EntityState;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

public class VacuumState implements EntityState {
    private float x,y;

    public VacuumState(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void advance(long deltaTime, Entity self, WPState wpState) {
        float speedX = (float) Math.sqrt(Math.pow(x - self.x, 2)) * Math.signum(x-self.x);
        float speedY = (float) Math.sqrt(Math.pow(y - self.y, 2)) * Math.signum(y-self.y);
        self.x += speedX * ((float) deltaTime) / 1000.0F;
        self.y += speedY * ((float) deltaTime) / 1000.0F;
    }

    @Override
    public void collide(CollisionAction.Direction d) {
        //TODO: Implement Me!
    }
}

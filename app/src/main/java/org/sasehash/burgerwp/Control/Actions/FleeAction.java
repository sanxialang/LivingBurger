/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.Actions;

import org.sasehash.burgerwp.Control.Action;
import org.sasehash.burgerwp.Control.EntityStates.Running;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.EntityFactory;
import org.sasehash.burgerwp.Model.WPState;

/**
 * Forget everything else and flee.
 */
public class FleeAction implements Action {
    private float x, y;

    public FleeAction(float x, float y) {
        this.x = x;
        this.y = y;
    }

    private float length(float x, float y, Entity e) {
        return (float) Math.sqrt(Math.pow(x - e.x, 2) + Math.pow(y - e.y, 2));
    }

    @Override
    public void update(WPState wpState) {
        for (Entity e : wpState.entities) {
            float distance = length(x, y, e);
            float speed = distance < 50 ? 75 : 50;
            e.state = new Running((e.x - x) / distance,
                    (e.y - y) / distance,
                    speed + EntityFactory.getSpeedVariation()
            );

        }

    }
}

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.Actions;

import org.sasehash.burgerwp.Control.Action;
import org.sasehash.burgerwp.Control.EntityStates.VacuumState;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

public class VacuumAction implements Action {
    private float x, y;

    public VacuumAction(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void update(WPState wpState) {
        for (Entity entity : wpState.entities) {
            entity.passiveStates.add(new VacuumState(x, y));
        }
    }
}

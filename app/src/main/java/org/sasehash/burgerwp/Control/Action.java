/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control;

import org.sasehash.burgerwp.Model.WPState;

public interface Action {
    void update(WPState wpState);
}

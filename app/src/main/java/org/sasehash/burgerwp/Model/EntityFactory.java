/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.sasehash.burgerwp.Control.EntityStates.Rotating;
import org.sasehash.burgerwp.Control.EntityStates.Running;
import org.sasehash.burgerwp.R;

import java.util.Random;

public class EntityFactory {
    public static Random r = new Random();

    public static float getSpeedVariation() {
        return (30.0F * r.nextFloat());
    }

    public static Entity burger(Context c) {
        Bitmap b = BitmapFactory.decodeResource(c.getResources(), R.drawable.burger);
        return fromBitmap(b, c);
    }

    public static Entity fromBitmap(Bitmap b, float rotation) {
        Entity e = new Entity(0.0F, 0.0F, 0, 1.0F, b.getWidth(), b.getHeight(), b);
        e.state = new Running(1.0F, 1.0F, 40.0F + getSpeedVariation());
        if (rotation != 0.0F) {
            e.passiveStates.add(new Rotating(rotation));
        }
        return e;
    }

    public static Entity fromBitmap(Bitmap b, Context c) {
        return fromBitmap(b, 0.0F);
    }
}

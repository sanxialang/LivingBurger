/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Model;

import android.graphics.Bitmap;

import org.sasehash.burgerwp.Control.EntityState;
import org.sasehash.burgerwp.Control.EntityStates.ProcrastinatingState;

import java.util.ArrayList;
import java.util.List;

public class Entity {
    public float x, y, rotation, scale, oldx, oldy;
    public float width, height;
    public Bitmap texture;
    public EntityState state = new ProcrastinatingState();
    public List<EntityState> passiveStates = new ArrayList<>();

    public Entity(float x, float y, float rotation, float scale, float width, float height, Bitmap texture) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.scale = scale;
        this.width = width;
        this.height = height;
        this.texture = texture;
    }


    public void advance(long deltaTime, WPState wpState) {
        float oldoldx = x;
        float oldoldy = y;
        for (EntityState state: passiveStates) {
            state.advance(deltaTime, this, wpState);
        }
        state.advance(deltaTime, this, wpState);
        oldx = oldoldx;
        oldy = oldoldy;

        if (x < 0) { x = 0; }
        if (y < 0) { y = 0; }
        if (x + width > wpState.screenSizeX) { x = wpState.screenSizeX - width -1; }
        if (y + height > wpState.screenSizeY) { y = wpState.screenSizeY - height -1; }
    }
}
